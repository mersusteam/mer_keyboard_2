﻿using UnityEngine;
using TMPro;

public class KeyScript : MonoBehaviour
{


    //Object to get the textmesh for caps option
    private TextMeshPro _textMesh;
    
    // Start is called before the first frame update
    private void Start()
    {
        GameObject obj = GameObject.Find(gameObject.name);
        _textMesh = obj.GetComponentInChildren<TextMeshPro>();
        _textMesh.text = obj.name.ToLower().Trim(' ');
    }
}