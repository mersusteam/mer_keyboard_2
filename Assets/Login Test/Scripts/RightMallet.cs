﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
//using Photon.Pun;

public class RightMallet : MonoBehaviour
{
    // Start is called before the first frame update
    bool pressed; //Boolean to check if the shift key is pressed
    public InputField input; //Field to pass the input field 
    string inp; //String to manipulate the input string
 
    void Start()
    {
       
    }

    // Update is called once per frame
    void OnTriggerExit(Collider other)
    {
        //Condition to check if the collider has exited the Shift key
        if (other.gameObject.name == "Shift")
        {
            pressed = false; //Setting the boolean to false for accessing the presetted key
            //Debug.Log("Boolean Check"+pressed.ToString());
           // key.DoKeyPress(KeyboardState.ShiftOff);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Shift")
        {
            Debug.Log("Check Shift Key Pressed");
            //key.DoKeyPress(KeyboardState.ShiftOn);
        }
        //This condition handles all the objects that do not need to be noticed.
        if (other.gameObject.name == "RightMallet" || other.gameObject.name == "LeftMallet" || other.gameObject.name == "LeftStick" || other.gameObject.name == "RightStick" || other.gameObject.name == "LeftHead" || other.gameObject.name == "RightHead" || other.gameObject.name == "[VRTK][AUTOGEN][Controller][CollidersContainer]" || other.gameObject.name == "[VRTK][AUTOGEN][BodyColliderContainer]" || other.gameObject.name == "Wall" || other.gameObject.name == "Floor" || other.gameObject.name == "Login Button")
        {
            
        }
        else if (other.gameObject.name == "Backspace")
        {//Code for Backsapce

            inp = input.text;//String to store the text entered in the Input Field in the scene
            //Condition to check if the string is not null and the length is more than 0
            if (inp != null && inp.Length > 0)
            {
                //Using subString to run through the String length and except the last character
                inp = inp.Substring(0, inp.Length - 1);
            }
            input.text = inp;//this line outputs the substring after the last character is deleted.
        }
        else if (other.gameObject.name == "Space")
        {//Code for the Space key

            input.text += " ";//Outputs the input text with the added space.
        }
        //code to check if the Shift key is pressed
        else if (other.gameObject.name == "Shift")
        {
            //Setting the boolean to true which would indicate the nature of the alphabet, i.e Caps.
            //KeyboardManager.DoKeyPress(KeyboardState.ShiftOn);
            Debug.Log("Shift key pressa");
        }
        else
        {//Not working it yet
         //Code to write the letters into the input field

            /*Debug.Log("Boolean Check" + pressed);
            if (pressed==true)// Check if the shift button is pressed to switch down to lowercase letters
            {
                other.gameObject.name = inp;//Getting the Game Onject name to a string
                Debug.Log("Boolean check:" + inp.ToLower());//Log to check the lower case letters
                input.text += other.gameObject.name.ToLower();//output the letters punched to the input field
            }
            else
            {//if the shift key is not pressed
                input.text += other.gameObject.name;//output the letters punched to the input field
                Debug.Log(other.gameObject.name.ToLower());
            }*/


            //boolCheck(other.gameObject.name);
            //KeyboardManager key = new KeyboardManager();
            //key.KeyPressed(other.gameObject.name, KeyboardState.ShiftOff);
        }
       


    }

    public void boolCheck(string inputField)
    {
        if (pressed==true)
        {
            string inn = inputField.ToLower();
            input.text += inn;
        }
        else
        {
            input.text += inputField;
        }
    }
}   
