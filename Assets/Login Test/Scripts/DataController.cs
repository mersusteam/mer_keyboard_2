﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using System.Security.Cryptography;
using System.Text;



public class DataController : MonoBehaviour
{
   
    [SerializeField] public GameObject credentialsAlert;
    [SerializeField ]public GameObject emptyAlert;
    // Start is called before the first frame update
    string[] usernameFromServer;
    string[] passwordFromServer;
    string hashValueUsername;
    string[] textArray;
    string[,] text2dArray;
    
    void Start()
    {
        StartCoroutine(GETData());

        
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #region Hash Functions

    //hash Function 
    //Algorith used: SHA256
    static string ComputeSha256Hash(string rawData)
    {
        // Create a SHA256   
        using (SHA256 sha256Hash = SHA256.Create())
        {
            // ComputeHash - returns byte array  
            byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

            // Convert byte array to a string   
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                builder.Append(bytes[i].ToString("x2"));
            }
            return builder.ToString();
        }
    }

    //The function is implemented as the normal hash is reversible and not that secure
    //its a web of hash, as SHA 256 is reversible to some extent.
    //using this logic would rule out every possibility of reversing the hash and getting the actual data

    public string hashValue(string rawData)
    {
        //Algorithm used - SHA 256

        //1) Get the length of the raw data
        //2)Hash the raw data (HashIntial)
        //3)Hash the length(int) of the raw data (hashLen)
        //4)Stuff 'hashLen' inside 'HashInitial' at the 7th place if the length of the rawdata is 7
        //5)Hash the stuffed string (finalValue)
        //return
        int stringLen = rawData.Length;
        string hashIntial = ComputeSha256Hash(rawData);
        string addHashPre = hashIntial.Substring(0, stringLen-1);
        Debug.Log(addHashPre);
        string addHashPost = hashIntial.Substring(stringLen- 1, stringLen - 2);
        string hashLen = ComputeSha256Hash(stringLen.ToString());
        Debug.Log("Final Hashed Value:"+ComputeSha256Hash(addHashPre+hashLen+addHashPost));
        string finalValue = ComputeSha256Hash(addHashPre + hashLen + addHashPost);
        return finalValue;
    }

    #endregion

    #region Getting Data From Server

    #region Get Data

    private IEnumerator GETData()
    {
        //URL from which the data will be retrieved
        using (UnityWebRequest www = UnityWebRequest.Get("http://localhost/index.php"))
        {
            yield return www.Send();
            //condition to check if any network error occur
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {//getting the data
                Debug.Log(www.downloadHandler.text);
                //getting the text from the server onto a string
                string getText = www.downloadHandler.text;
                //spliting the text as it is coded with `|`, indicates end of a hash value
                textArray = getText.Split('/');
                //storing the string into arrays              
                usernameFromServer = textArray[0].Split('|');
                passwordFromServer = textArray[1].Split('|') ;
                Debug.Log("Username is:" +usernameFromServer +"Password is:"+passwordFromServer);
            }
        }
       
    }
    #endregion

    

    public void ReadData(string name,string password)
    {
        if (name == "" && password == "")
        {
            //makes the empty alert visible in the scene
            emptyAlert.transform.GetChild(0).GetComponent<Text>().enabled = true;
        }
        else if (name == "" || password == "")
        {
            emptyAlert.transform.GetChild(0).GetComponent<Text>().enabled = true;
        }
        
        else
        {
            Debug.Log("Else statement");
            for (int i = 0; i < usernameFromServer.Length; i++)
            {

                if (usernameFromServer[i].ToString() == hashValue(name) && passwordFromServer[i].ToString() == hashValue(password))
                {
                    Debug.Log("Username from Server (Loop):" + usernameFromServer[i] + "Password from Server (Loop):" + passwordFromServer[i]);
                    Debug.Log("Matched");
                    SceneManager.LoadScene("MainScene");
                }
                else if (textArray[i].ToString() != hashValue(name) || textArray[i].ToString() != hashValue(password))
                {//if the credentials do not match to the ones in the database make the Wrong credentials alert visible
                    credentialsAlert.transform.GetChild(0).GetComponent<Text>().enabled = true;
                }
                else
                {
                    Debug.Log("Username from Server (Loop):" + usernameFromServer[i] + "Password from Server (Loop):" + passwordFromServer[i]);
                    Debug.Log("Nahi hua yaar");
                }


            }
        }


        #region Getting Data From Mysql Database
        /*Debug.Log("Inside read json Username is:"+usernameFromServer +"Password is"+passwordFromServer);
        //string file = "Assets/Login Test/Data/data.json";
        //condition for empty inboxes
        if (name == "" && password == "")
        {
            //makes the empty alert visible in the scene
            emptyAlert.transform.GetChild(0).GetComponent<Text>().enabled = true;
        }
        else if (name == "" || password == "")
        {
            emptyAlert.transform.GetChild(0).GetComponent<Text>().enabled = true;
        }
        //condition to check if the username enter match with the ones in the database
        else if (textArray[0].ToString() == hashValue(name) && textArray[1].ToString() == hashValue(password))
        {//if yes then load a new scene
            SceneManager.LoadScene("MainScene");
        }//condition for wrong credentials response
        else if (textArray[0].ToString() != hashValue(name) || textArray[1].ToString() != hashValue(password))
        {//if the credentials do not match to the ones in the database make the Wrong credentials alert visible
            credentialsAlert.transform.GetChild(0).GetComponent<Text>().enabled = true;
        }*/
        #endregion

        #region Getting Data From Json
        //if (File.Exists(file))
        //{
        /* Debug.Log("File Exists");
         string dataAsJson = File.ReadAllText(file);
         Debug.Log(dataAsJson);
         GameData dataIn = JsonUtility.FromJson<GameData>(dataAsJson);
         Debug.Log("Name is:"+dataIn.name);
         Debug.Log("name"+name);
         Debug.Log("Pass"+password);
         if (name == "" && password == "")
         {
             emptyAlert.transform.GetChild(0).GetComponent<Text>().enabled = true;
         }
         else
         {
             if (dataIn.name != name)
             {
                 usernameAlert.transform.GetChild(0).GetComponent<Text>().enabled = true;
             }
             if (dataIn.password != password)
             {
                 passwordAlert.transform.GetChild(0).GetComponent<Text>().enabled = true;
             }
         }
    //}
    else
    {
        Debug.Log("File Unreachable");
    }*/

        #endregion
    }

    #endregion
}

//Class needed when getting data from json.
/*[System.Serializable]
public class GameData {
    public string name;
    public string password;
}*/
