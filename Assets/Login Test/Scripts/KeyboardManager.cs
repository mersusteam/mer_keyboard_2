﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public enum KeyboardState : byte
{
    ShiftOn,
    ShiftOff,
    CapsOn,
    SymbolsOn,
    SymbolsOff,
    General,
    FollowOff,
    // Add more
}

/// <summary>
/// This manages the state of the keyboard e.g. 
/// </summary>
public class KeyboardManager : MonoBehaviour
{
    //public InputField input;
    #region Variables

    [SerializeField] public KeyboardState mKeyboardState = KeyboardState.ShiftOff;
    //public KeyboardState m_KeyboardState = KeyboardState.ShiftOn;
    [SerializeField] private GameObject alphabets;
    [SerializeField] private GameObject symbols;
    public InputField inn;
    public InputField innPassword;
    public Text transparentField;
    private string _name;
    public GameObject panel;

    private bool _mKeyPressCooldownBool = false;
    private string _mTempInput;
    private KeyboardState _mTempState;
    // event 
    // delegate

    // public static 
    // KeyboardManager.DoKeyPress(string _input);

    //public List<GameObject> m_Rows;

    //public List<GameObject> m_Keys;


    #endregion

    #region init
    // Start is called before the first frame update
    private void Start()
    {
        //List<GameObject> m_TempList = new List<GameObject>();
        //foreach(GameObject o in m_Rows)
        //{
        //    int x = o.transform.childCount;
        //    for (int i = 0; i < x; i++)
        //    {
        //        m_Keys.Add(o.transform.GetChild(i).gameObject);
        //    }
        //}
        //transparentField.enabled = false;
        panel.SetActive(false);
        
    }
    #endregion

    #region Functions
    /// <summary>
    /// This comes from an unreferenced place. e.g. from a keyboard key
    /// </summary>
    public void DoKeyPress(KeyboardState input)
    {
        
        Debug.Log("In DoKeyPress Function");
        mKeyboardState = input;
        // do logic here to determine what needs to happen,
        // e.g. if shift, change state, OR, if "H", check state and write appropriate key. 
        
    }

    
    public void KeyPressed(string inputS, KeyboardState inp) // keycode or string. 
    {
        if(!_mKeyPressCooldownBool)
        {
            _mKeyPressCooldownBool = true;

            //m_KeyboardState = inp;
            _mTempInput = inputS;
            _mTempState = inp;

            StartCoroutine(nameof(KeyPressWait));
        }
    }

    private IEnumerator KeyPressWait()
    {
        for (int i = 0; i < 7; i++)
            yield return 0;
        
        mKeyboardState = _mTempState;
        panel.SetActive(true);

        //OnPayloadAction(inp);
        Debug.Log("Check Do Key Press " + _mTempInput);
        Debug.Log("Input State " + _mTempState);
        switch (_mTempState)
        {
            //conditions to check which button is pressed
            //condition to check if the symbol is pressed, and the enum is set to Symbols is On.
            case KeyboardState.SymbolsOn:
                //getting the gameobjects that need to be hid- Alphabets
                //GameObject objectToHide = GameObject.Find("Keyboard & Login Button/Keyboard/Canvas/KeyBoard Setup/Keyboard/Keyboard - Canvas/Alphabets");
                alphabets.SetActive(false);

                //getting the gameobjects that need to set to visible - Symbols
                //GameObject objectToShow = GameObject.Find("Keyboard & Login Button/Keyboard/Canvas/KeyBoard Setup/Keyboard/Keyboard - Canvas/Symbols");
                symbols.SetActive(true);
                mKeyboardState = KeyboardState.ShiftOff;
                break;
            //condition to check if the symbol is pressed and toggle it
            //first check point in the nest
            case KeyboardState.SymbolsOff:
                //getting the gameobjects that need to be hid - Symbols
                //GameObject objectToShow = GameObject.Find("Keyboard & Login Button/Keyboard/Canvas/KeyBoard Setup/Keyboard/Keyboard - Canvas/Symbols");
                symbols.SetActive(false);

                //getting the gameobjects that need to be set to visible - Alphabets
                //GameObject objectToHide = GameObject.Find("Keyboard & Login Button/Keyboard/Canvas/KeyBoard Setup/Keyboard/Keyboard - Canvas/Alphabets");
                alphabets.SetActive(true);

                //setting the keyboardstate to shift off
                mKeyboardState = KeyboardState.ShiftOff;
                break;
            case KeyboardState.ShiftOn:
                Debug.Log("Inp Check:" + _mTempState.ToString());
                switch (_mTempInput)
                {
                    //Debug.Log("Input Check:"+input.text.ToString()+ "Check");
                    //for the backspace
                    case "Backspace":
                    {
                        _mTempInput = inn.text;//String to store the text entered in the Input Field in the scene
                        //Condition to check if the string is not null and the length is more than 0         
                
                
                        if (!string.IsNullOrEmpty(_mTempInput))
                        {
                            //Using subString to run through the String length and except the last character
                            _mTempInput = _mTempInput.Substring(0, _mTempInput.Length - 1);
                        }
                
                        inn.text = _mTempInput;
                        break;
                    }
                    case "Space":
                        inn.text += " ";
                        break;
                    default: //this condition fulfills for the all the alphabets and the symbols
                        //writes the text to the inputField in the scene
                        inn.text += _mTempInput;
                        transparentField.text = _mTempInput;
                        break;
                }

                break;
            case KeyboardState.ShiftOff when _mTempInput == "Space":
                inn.text += " ";
                break;
            case KeyboardState.ShiftOff when _mTempInput == "InputFieldName":
            {
                //to set the inputField to the username
                Debug.Log("In Input Field");
                InputField inputField = GameObject.Find("InputFieldName").GetComponent<InputField>();
                InputField password = GameObject.Find("InputFieldPassword").GetComponent<InputField>();
                inn = inputField;
                inn.image.color = Color.gray;
                password.image.color = Color.white;
                break;
            }
            case KeyboardState.ShiftOff when _mTempInput == "Backspace":
            {
                _mTempInput = inn.text;//String to store the text entered in the Input Field in the scene
                //Condition to check if the string is not null and the length is more than 0
                if (!string.IsNullOrEmpty(_mTempInput))
                {
                    //Using subString to run through the String length and except the last character
                    _mTempInput = _mTempInput.Substring(0, _mTempInput.Length - 1);
                }
                inn.text = _mTempInput;
                transparentField.text = _mTempInput;
                break;
            }
            case KeyboardState.ShiftOff when _mTempInput == "InputFieldPassword":
            {
                //to set the inputField to the password
                //name = inn.text;
                InputField password = GameObject.Find("InputFieldPassword").GetComponent<InputField>();
                InputField inputField = GameObject.Find("InputFieldName").GetComponent<InputField>();
                inn = password;
                password.image.color = Color.gray;
                inputField.image.color = Color.white;
                break;
            }
            case KeyboardState.ShiftOff when _mTempInput == "LoginButton":
            {
                //string name = inn.text;
                InputField inName = GameObject.Find("InputFieldName").GetComponent<InputField>();
                InputField inPass = GameObject.Find("InputFieldPassword").GetComponent<InputField>();
                //DataController dC = new DataController();
                //getting the already created object and calling the functions through it
                DataController dC = GetComponent<DataController>();
                dC.ReadData(inName.text, inPass.text);
                break;
            }
            case KeyboardState.ShiftOff:
                inn.text += _mTempInput.ToLower();
                transparentField.text = _mTempInput;
                break;
            default:
                Debug.Log("Nothings Happening");
                break;
        }

        // flip back on the boolean, allowing another input. 
        _mKeyPressCooldownBool = false;
    }

    public void TypeInput(string input)
    {
        KeyPressed(input, mKeyboardState);
    }
    
    #endregion
}
