﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LeftMallet : MonoBehaviour
{
    // Start is called before the first frame update
    bool pressed = false;//Boolean to check the state of the shift key
    public InputField input;//input field to get the user input 
    string inp = ""; // string to be used in text manipulations
    void Start()
    {

    }

    
    void OnTriggerExit(Collider other)
    {//code to check if the mallet has exited the shift key
        if (other.gameObject.name == "Shift")
        {
            pressed = true;//change the boolean state to false, denoting that the shift key is not pressed
            Debug.Log(pressed.ToString());
        }
    }

    void OnTriggerEnter(Collider other)
    {
     //condition to ignore all the objects that don't need to be recognized  
        if (other.gameObject.name == "RightMallet" || other.gameObject.name == "LeftMallet" || other.gameObject.name == "LeftStick" || other.gameObject.name == "RightStick" || other.gameObject.name == "LeftHead" || other.gameObject.name == "RightHead" || other.gameObject.name == "[VRTK][AUTOGEN][Controller][CollidersContainer]" || other.gameObject.name == "[VRTK][AUTOGEN][BodyColliderContainer]" || other.gameObject.name == "Wall" || other.gameObject.name == "Floor" || other.gameObject.name == "Login Button")
        {
            //do nothing
        }
        else if (other.gameObject.name == "Backspace")
        {//condtion to act on the press of Backspace

            inp = input.text;// storing the text in the input box for further manipulation
            if (inp != null && inp.Length > 0)// checking if the string is not null and if the length in greater than 0
            {//using the substring method to run through the whole string except the last character, implements backspace
                inp = inp.Substring(0, inp.Length - 1);
            }
            input.text = inp;//write the string to the input field.
        }
        else if (other.gameObject.name == "Space")
        {//section implements the space key
            input.text += " ";
        }
        else if (other.gameObject.name == "Shift")
        {//Implemets the shift key
            pressed = true;
            //sets the boolean state to true, denoting that the shift key is pressed
            Debug.Log("Boolean Check:"+pressed.ToString());

        }
        else
        {//Not working it yet
         //C Debug.Log("Boolean Check" + pressed);
            if (pressed==true)// Check if the shift button is pressed to switch down to lowercase letters
            {
                inp = other.gameObject.name;//Getting the Game Onject name to a string
                Debug.Log("Boolean check:" + inp.ToLower());//Log to check the lower case letters
                input.text += other.gameObject.name.ToLower();//output the letters punched to the input field
            }
            else
            {//if the shift key is not pressed
                input.text += other.gameObject.name;//output the letters punched to the input field
            }


        }

    }    
}   
