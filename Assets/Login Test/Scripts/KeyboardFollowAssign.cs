﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public enum KeyboardFollow : byte
{
    StartFollow,
    StopFollow,
}
public class KeyboardFollowAssign : MonoBehaviour
{

    [SerializeField] public KeyboardFollow m_keyboardFollow = KeyboardFollow.StopFollow; 
    #region Events
    public delegate void PayloadAction(KeyboardFollow follow);
    public static event PayloadAction OnPayloadAction;
    

    int i = 150;
    #endregion
    private void FixedUpdate()
    {
        
        if (i>=30 && i<=150)
        {
            m_keyboardFollow = KeyboardFollow.StopFollow;
            
        }
        else if(i>=0 && i<30){
            m_keyboardFollow = KeyboardFollow.StartFollow;
        }
        
        i--; if (i == 0) i = 150;
        Debug.Log("i Count"+i);
        DoThing();
    }
    public void ChangeState()
    {
        
        for(int j=0;j<=5;j++)
            i = 150;
    }

    private void DoThing()
    {
        Debug.Log("In the DoThing");
        
        if (m_keyboardFollow == KeyboardFollow.StartFollow)
        {
            GetComponent<VRTK_TransformFollow>().gameObjectToFollow = GameObject.FindGameObjectWithTag("MainCamera");
            //Vector3 up = GetComponent<VRTK_TransformFollow>().FollowMoment.OnFixedUpdate;
            //Quaternion rotation = Quaternion.LookRotation(GetComponent<VRTK_TransformFollow>().gameObjectToFollow);
  
        }
        else if (m_keyboardFollow == KeyboardFollow.StopFollow)
        {
            GetComponent<VRTK_TransformFollow>().gameObjectToFollow = null;
        }
    }
    
}
