﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Mallet : MonoBehaviour
{
    //public InputField input;

    //gameObjects to get the alerts in the scene
    [SerializeField] public GameObject credentialsAlert;

    [SerializeField] public GameObject emptyAlert;

    //Object for the KeyboardManager script which can be used throughout (Singleton)
    public KeyboardManager key;

    public KeyboardFollowAssign keyFollow;

    //Booleans for symbols and Caps
    private bool _symCheck;

    private bool _capsCheck;

    // Start is called before the first frame update
    private void Start()
    {
        //Find and set the Keyboard Manager
        GameObject obj = GameObject.Find("Keyboard Setup");
        key = obj.GetComponent<KeyboardManager>();
    }

    //method to hide the alerts on press of the inputFields
    private void HideAlerts()
    {
        emptyAlert.transform.GetChild(0).GetComponent<Text>().enabled = false;
        credentialsAlert.transform.GetChild(0).GetComponent<Text>().enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        keyFollow.ChangeState();
        switch (other.gameObject.name)
        {
            //All the objects that need to be ignored when the collision occurs
            case "Plane":
            case "Mesh":
            case "RightController":
            case "LeftController":
            case "[VRTK][AUTOGEN][FootColliderContainer]":
            case "Sphere":
            case "RightMallet":
            case "LeftMallet":
            case "LeftStick":
            case "RightStick":
            case "LeftHead":
            case "RightHead":
            case "[VRTK][AUTOGEN][Controller][CollidersContainer]":
            case "[VRTK][AUTOGEN][BodyColliderContainer]":
            case "Wall":
            case "Floor":
            case "ControllerRigidbodyActivator":
            case "[VRTK][AUTOGEN][LeftController][StraightPointerRenderer_Cursor]":
            case "[VRTK][AUTOGEN][RightController][StraightPointerRenderer_Cursor]":
            case "[VRTK][AUTOGEN][RightController][StraightPointerRenderer_Tracer]":
            case "[VRTK][AUTOGEN][LeftController][StraightPointerRenderer_Tracer]":
                break;
            case "Symbol":
            {
                //If the collision occurs with symbol key
                GetComponent<AudioSource>().Play(); //play the button sound
                Debug.Log("SYMBOLS PRESSED");
                //condition to toggle symbols 
                if (_symCheck == false)
                {
                    _symCheck = true;
                    key.KeyPressed(other.gameObject.name, KeyboardState.SymbolsOn);
                }
                else
                {
                    _symCheck = false;
                    key.KeyPressed(other.gameObject.name, KeyboardState.SymbolsOff);
                }

                break;
            }
            case "Caps":
            {
                GetComponent<AudioSource>().Play();
                //Condition to toggle the caps lock
                if (_capsCheck == false)
                {
                    _capsCheck = true;
                    key.DoKeyPress(KeyboardState.ShiftOn);
                    //key.KeyPressed(other.gameObject.name, KeyboardState.ShiftOn);
                }
                else
                {
                    _capsCheck = false;
                    //key.KeyPressed(other.gameObject.name, KeyboardState.ShiftOff);
                    key.DoKeyPress(KeyboardState.ShiftOff);
                }

                break;
            }
            case "Backspace":
                //For Backspace
                GetComponent<AudioSource>().Play();
                key.KeyPressed(other.gameObject.name, key.mKeyboardState);
                break;
            case "Space":
                //for space
                GetComponent<AudioSource>().Play();
                key.KeyPressed(other.gameObject.name, key.mKeyboardState);
                break;
            case "InputFieldName":
                //When the inputFields is clicked
                HideAlerts(); //hide the alerts if they are visible
                GetComponent<AudioSource>().Play();
                key.KeyPressed(other.gameObject.name, key.mKeyboardState);
                break;
            case "InputFieldPassword":
                HideAlerts(); //hide the alerts if they are visible
                GetComponent<AudioSource>().Play();
                key.KeyPressed(other.gameObject.name, key.mKeyboardState);
                break;
            case "LoginButton":
                //on login button press, gets data from the inputFields and checks it with the data from the server
                GetComponent<AudioSource>().Play();
                key.KeyPressed(other.gameObject.name, key.mKeyboardState);
                break;
            default:
                GetComponent<AudioSource>().Play();
                //key.KeyPressed(other.gameObject.name, KeyboardState.ShiftOff);
                //input.text += other.gameObject.name;
                key.TypeInput(other.GetComponentInChildren<TextMeshPro>().text);
                break;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        keyFollow.ChangeState();
        if (other.gameObject.name == "Backspace")
        {
            key.KeyPressed(other.gameObject.name, key.mKeyboardState);
        }
    }
}